from pathlib import Path
import shutil
from Exceptions import *


class Saves:

    def __init__(self, directory: str):
        self.directory = directory

    def set_save_directory(self, directory):
        self.directory = directory

    def get_save_directory(self):
        return self.directory

    def save_state(self, name, replace=False):
        save_path = Path("./saves")

        if not save_path.is_dir():
            save_path.mkdir()

        if self.directory is not None:
            p = Path(f'{save_path}/{name}.sl2')
            if p.is_file():
                if not replace:
                    raise SaveAlreadyExists()
            elif not p.is_file():
                save_p = Path(f'{self.directory}/ER0000.sl2')
                if not save_p.is_file():
                    raise NoSaveDataFound()
            shutil.copy(f'{self.directory}/ER0000.sl2', f'./saves/{name}.sl2')

        else:
            raise Exception("Save directory is not defined")

    def delete_save_state(self, save):
        save_path = Path("./saves")
        p = Path(f'{save_path}/{save}.sl2')
        if p.is_file():
            p.unlink()

    def load_save_state(self, save):
        shutil.copy(f'./saves/{save}.sl2', f'{self.directory}/ER0000.sl2')
        p = Path(f'{self.directory}/ER0000.sl2.bak')
        if p.is_file():
            p.unlink()
