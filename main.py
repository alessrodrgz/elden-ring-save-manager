import os
import sys
import PySimpleGUI as sg
from Config import Config
from Exceptions import *
from Saves import Saves


def resource_path(relative_path):
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS, relative_path)

    return os.path.join(os.path.abspath("."), relative_path)

sg.theme('DarkAmber')

cfg = Config()
sv = Saves(cfg.get_save_directory())

layout = [
    [sg.Text("Save location:"),
     sg.In(cfg.get_save_directory(), size=(25, 1), enable_events=True, key="save-directory", disabled=True),
     sg.FolderBrowse()],
    [sg.Listbox(cfg.get_saves(), size=(45, 10), key="selected-save")],
    [sg.Button("Save state", key="save-state"), sg.Button("Load state", key="load-state"),
     sg.Button("Delete state", key="delete-state")]
]

window = sg.Window('Elden Ring - Save manager', layout, icon=resource_path("icon.ico"))
saves_list = window.find_element("selected-save")


def save_sate():
    save_name_set = False
    save_name = None
    while not save_name_set:
        save_name = sg.popup_get_text("Save name:", no_titlebar=True, modal=True)

        if save_name is None:
            break
        elif not save_name:
            sg.popup(f'Save name is required', no_titlebar=True, modal=True)
        elif not save_name.strip():
            sg.popup(f"Save name can't be blank", no_titlebar=True, modal=True)
        else:
            save_name = save_name.strip()
            save_name_set = True

    if save_name is not None:
        try:
            sv.save_state(save_name)
            cfg.add_save(save_name)
        except NoSaveDataFound as e:
            sg.popup_error(e.message)
        except SaveAlreadyExists as e:
            replace = sg.popup_ok_cancel(f'{e.message} Do you want to replace it?')
            replace = True if replace == "OK" else False

            if replace:
                sv.save_state(save_name, replace=replace)
                cfg.add_save(save_name)
        finally:
            saves_list.Update(values=cfg.get_saves())


def delete_state(states):
    if len(states) > 0:
        sv.delete_save_state(states[0])
        cfg.remove_save(states[0])
        saves_list.Update(values=cfg.get_saves())
    else:
        sg.popup_error("You must select a save state to delete.", no_titlebar=True, modal=True)


def load_state(saves):
    if len(saves) > 0:
        save = saves[0]
        sv.load_save_state(save)
        sg.popup_auto_close("Save state loaded.", no_titlebar=True, button_type=5)
    else:
        sg.popup_error("You must select a save state to load.", no_titlebar=True, modal=True)


while True:
    event, values = window.Read()

    if event in (sg.WINDOW_CLOSED, 'Exit'):
        break
    elif event == 'save-directory':
        cfg.set_save_directory(values["save-directory"])
        sv.set_save_directory(values["save-directory"])
    elif event == "save-state":
        save_sate()
    elif event == "delete-state":
        delete_state(values["selected-save"])
    elif event == "load-state":
        load_state(values["selected-save"])
