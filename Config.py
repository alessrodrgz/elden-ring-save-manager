import json
import os


def load_config_file(path):
    with open(path, 'r') as f:
        return json.load(f)


class Config:

    def __init__(self):
        if not os.path.exists('./config.json'):
            with open('./config.json', 'w+') as f:
                json.dump({}, f)
                f.close()
        self.config = load_config_file('./config.json')
        self.config["saves"] = self.config["saves"] if "saves" in self.config else []

    def update_json(self):
        with open('./config.json', 'w') as f:
            json.dump(self.config, f)
            f.close()

    def set_save_directory(self, directory: str):
        self.config["save-directory"] = directory
        self.update_json()

    def get_save_directory(self):
        return self.config["save-directory"] if "save-directory" in self.config else None

    def get_saves(self):
        return self.config["saves"]

    def add_save(self, save):
        self.config["saves"].append(save)
        self.update_json()

    def remove_save(self, save):
        self.config["saves"].remove(save)