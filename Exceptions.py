

class SaveManagerError(Exception):
    # Base Exception class
    pass


class SaveAlreadyExists(SaveManagerError):
    # Raised when there's a save with the same name
    def __init__(self):
        self.message = "There's a save with the same name."

    pass


class NoSaveDataFound(SaveManagerError):
    # Raised when Elden Ring savedata is not found on the specified directory
    def __init__(self):
        self.message = "There's no Elden Ring savedata on the specified directory."

    pass
